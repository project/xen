<?php


/**
 Helper for Perl interface to list not created projects
 */

include_once './includes/bootstrap.inc';
drupal_bootstrap(DRUPAL_BOOTSTRAP_FULL);

ini_set('max_execution_time', 0);
if (ini_get('max_execution_time') > 0) {
 watchdog('xen', t('Host creation failed: Not enough time. Please your PHP settings'));
}

// Only list one host per run
$res = db_query("SELECT * FROM {xen} WHERE created IS NULL ORDER BY xid ASC LIMIT 1");
$row = db_fetch_array($res);
if (!empty($row)) {
  if (!db_query("UPDATE {xen} SET created = 1 WHERE xid = '%d'", $row['xid'])) {
    watchdog('xen', 'Updating pending host failed, check database');
    exit;
  }

  $parts = preg_split("/\./", $row['ip']);
  $gw = $parts[0] . '.' . $parts[1] . '.' . $parts[2] . '.' . ($parts[3] + 127);

              //--gateway=". escapeshellarg($gw) ." \
  $command = "xen-create-image  \
              --hostname=" . escapeshellarg(check_plain($row['name'])) . " \
              --dir=" . escapeshellarg(variable_get('xen_servers_path', '')) ." \
              --dist=". escapeshellarg(check_plain($row['distro'])) ." \
              --ip=". escapeshellarg($row['ip']) ." \
              --gateway=147.32.80.6 \
              --netmask=255.255.255.0 \
              --memory=". escapeshellarg($row['memory']."Mb"). " \
              --size=". escapeshellarg($row['disk'].'Gb') ." \
              --swap=". escapeshellarg($row['swap'].'Mb') ." \
              --fs=". escapeshellarg(variable_get('xen_fs', 'ext3')) ." \
              --role=ssh --boot ";
  if (variable_get('xen_disktype', 'ide') == 'ide') {
    $command .= "--ide";
  }

  // Create the host.
  exec("$command", $output);
  print_r($output);

  // Creation should be done now, determine SSH key and send email to user
  $sshkey = @file_get_contents(variable_get('xen_key_dir', '/home/xen/keys'). '/' . $row['name']);
  // Remove SSH Key as soon as possible.
  unlink(variable_get('xen_key_dir', '/home/xen/keys'). '/' . $row['name']);

  // Determine recipient.
  $owner = user_load(array('uid' => $row['uid']));

  $to = $owner->mail;
  $subject = t('New XEN domain created');
  $body = t("Your new XEN domain has been created.\n\nIP Address: @ip\nHostname: @hostname\n\nYou can now login to your new host using the following SSH Key:\n!sshkey\n\nPlease change root password of your host immediatelly!\nPlease delete the key above from /root/.ssh/authorized_keys2 immediatelly!", array('@ip' => $row['ip'], '@hostname' => $row['name'], '!sshkey' => $sshkey));

  drupal_mail('xen_host_created', $to, $subjet, $body);

  db_query("UPDATE {xen} SET created = '%d' WHERE xid = '%d'", mktime(), $row['xid']);
}
