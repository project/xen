DROP TABLE IF EXISTS `xen`;
CREATE TABLE `xen` (
  `xid` int(11) NOT NULL auto_increment,
  `name` varchar(100) collate utf8_czech_ci default NULL,
  `ip` char(15) collate utf8_czech_ci NOT NULL,
  `disk` int(11) default NULL,
  `swap` int(11) default NULL,
  `memory` int(11) default NULL,
  `pid` int(11) default NULL,
  `uid` int(11) default NULL,
  `created` int(11) default NULL,
  `distro` varchar(25) collate utf8_czech_ci default NULL,
  PRIMARY KEY  (`xid`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci;

DROP TABLE IF EXISTS `xen_ip`;
CREATE TABLE `xen_ip` (
  `iid` int(11) NOT NULL auto_increment,
  `ip_start` varchar(64) default NULL,
  `ip_end` varchar(64) default NULL,
  PRIMARY KEY  (`iid`)
) ENGINE=MyISAM AUTO_INCREMENT=49 DEFAULT CHARSET=latin1;
